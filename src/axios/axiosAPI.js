import axios from 'axios';

export default axios.create({
    baseURL:'http://18.219.124.253/api/',
    headers:{
        'Content-Type':'application/json',
        // 'Content-Type':'application/x-www-form-urlencoded',
        'Accept':'application/json',
        'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept',
        // 'Access-Control-Allow-Methods':'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        'Cache-Control':'no-cache, no-store, must-revalidate',
        'Pragma':'no-cache',
        'Expires':0
    },
    timeout:60000
})