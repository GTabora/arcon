import React, { useContext, useEffect } from "react";
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonImg,
  IonAvatar,
  IonItem,
  IonCardContent,
  IonLabel,
  IonText,
  IonCol,
} from "@ionic/react";
import "./Perfiles.css";
import "./../../theme/variables.css";
import "../../theme/label.css";
import { Redirect } from "react-router";
import { UserContext } from "../../App";
import { Animated } from "react-animated-css";

type ProfileProps = {
  ListaPerfiles: any[];
};

const Perfiles: React.FC<ProfileProps> = ({ ListaPerfiles }) => {
  const user = useContext(UserContext);

  const renderProfiles = () => {
    return (
      ListaPerfiles &&
      ListaPerfiles.map((x) => {
        return (
          <IonCol size="6">
            <Animated
              animationIn="bounceInLeft"
              animationOut="fadeOut"
              isVisible={true}
              animationInDuration={1000}
            >
              <IonCard
                color={x.rolId == 1 ? "orange" : "lightblue"}
                routerLink="/goals"
                onClick={() => goToHome(x)}
                style={{ width: "90%", marginLeft: "5%" }}
              >
                <IonCardHeader>
                  <IonCardSubtitle>
                    <IonLabel color="smoke">{x.rolId == 1 ? "Propietario" : ""}</IonLabel>
                  </IonCardSubtitle>
                  <IonCardTitle>
                    <IonItem color="transparent" lines="none">
                    <img
                          src={
                            "../../assets/avatars/" + getRandomAvatar() + ".png"
                          }
                        />
                    </IonItem>
                  </IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                  <IonLabel className="profileName" color="smoke">
                  {x.name}
                  </IonLabel>
                </IonCardContent>
              </IonCard>
            </Animated>
          </IonCol>
        );
      })
    );
  };

  const getRandomAvatar = () => {
    const min = 1;
    const max = 31;

    return min + Math.floor(Math.random() * Math.floor(max));
  };

  const goToHome = (item: any) => {
    user.profileSelected = item;
  };

  return <>{renderProfiles()}</>;
};

export default Perfiles;
